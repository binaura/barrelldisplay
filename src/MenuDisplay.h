//
//  MenuDisplay.h
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 04..
//

#ifndef MenuDisplay_h
#define MenuDisplay_h

#include "ofMain.h"
#include "MenuTitle.h"

class MenuDisplay
{
public:
    void setup(ofRectangle view);
    void update();
    void draw();
    
    void next();
    void prev();    
    
    vector<MenuTitleRef> mTitles;
    int mCurrentTitleIndex;
    int mPrevTitleIndex;
    float mSwipePct, mSwipeDamp;
    float mTitleWidth;
    float mFgFadeAlpha, mFgFadeTargetAlpha;
    ofRectangle mView;
};

#endif /* MenuDisplay_h */
