#include "ofApp.h"
#include "RectTransform.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetCircleResolution(64);
    ofBackground(0);
    //ofEnableNormalizedTexCoords();
    
    ofLoadImage(mTexLoading, "loading.jpg");
    mTexLoading.setAnchorPercent(.5, .5);
    mOutputView.set(0, 0, 1280, 1024);
    
    mFboComp.allocate(mOutputView.getWidth(), mOutputView.getHeight(), GL_RGB);
    mFadeDir = 0;
    mFadeAlpha = 1;
    
    mMenuDisplay.setup(mOutputView);
    mWater.setup(mOutputView.getWidth(), mOutputView.getHeight());
    
    int bufferSize = 256;
    mSmoothedVol = 0;
    
    mStream.printDeviceList();
#ifdef TARGET_LINUX_ARM
    if (mStream.getDeviceList().size() == 4)
    {
        mStream.setDeviceID(2);
        mStream.setup(this, 0, 1, 44100, bufferSize, 4);
#else
        {
            mStream.setup(this, 0, 2, 44100, bufferSize, 4);
#endif
            
            mStream.start();
        }
        
        mParams.add(mScreenRotation.set("rotation", 0, 0, 360));
        mParams.add(mVolThreshold.set("vol threshold", 0.015, 0, 0.1));
        mParams.add(mWaterDisturbForceMax.set("water force", 1, 0, 2));
        mParams.add(mFadeSpeed.set("fade speed", 0.05, 0.001, 0.5));        
        
        ofXml xml; if (xml.load("settings.xml"))
        {
            xml.deserialize(mParams);
        }
        
        mGui.setup();
        mGui.setPosition(300, 100);
        mGui.add(mParams);
        mDisplayGui = false;
        
        mRPi.setup();
        ofAddListener(mRPi.onPressedEvent, this, &ofApp::onButtonPressedEvent);
        
        ofHideCursor();
    }
    
    //--------------------------------------------------------------
    void ofApp::update()
    {
        mRPi.update();
        mFboComp.begin();
        ofClear(0);
        ofSetColor(255);
        mFadeAlpha = ofClamp(mFadeAlpha + mFadeDir * mFadeSpeed, 0, 1);
        
        if (mFadeAlpha == 0 && mFadeDir != 0)
        {
            mSlidesDisplay = mNextSlidesDisplay;
            if (mSlidesDisplay)
            {
                mSlidesDisplay->load();
            }
            mNextSlidesDisplay = SlidesDisplayRef();
            mFadeDir = 1;
        }
        if (mFadeAlpha == 1)
        {
            mFadeDir = 0;
        }
        
        if (mSlidesDisplay)
        {
            mSlidesDisplay->update();
            mSlidesDisplay->draw();
        }
        else
        {
            mMenuDisplay.update();
            mMenuDisplay.draw();
        }
        
        mFboComp.end();
        
        if (mSmoothedVol > mVolThreshold)
        {
            ofVec2f p = ofVec2f(mOutputView.getWidth() / 2., mOutputView.getHeight() / 2.) + ofVec2f(ofSignedNoise(ofGetElapsedTimef(), 123.456), ofSignedNoise(ofGetElapsedTimef(), 987.765)) * 100;
            mWater.disturb(p, 100, ofMap(mSmoothedVol, 0, 1, 0, mWaterDisturbForceMax, true));
        }
        mWater.update();
    }
    
    //--------------------------------------------------------------
    void ofApp::draw()
    {
        
        RectTransform::begin(mOutputView);
        ofPushMatrix();
        ofTranslate(mOutputView.getCenter());
        ofRotate(mScreenRotation, 0, 0, 1);
        ofTranslate(-mOutputView.getWidth() / 2., -mOutputView.getHeight() / 2.);
        
        if (mFadeAlpha < 1)
        {
            ofSetColor(255);
            mTexLoading.draw(mOutputView.getCenter());
            /*
            float fontSize = 50;
            string text = "loading...";
            ofRectangle bbox = MenuTitle::mFont.getBBox(text, fontSize, 0, 0);
            MenuTitle::mFont.draw(text, fontSize, mOutputView.width / 2. - bbox.width / 2., mOutputView.height / 2. - bbox.height / 2.);
             */
            
        }
        
        ofSetColor(255, ofMap(mFadeAlpha, mFadeSpeed * 2, 1, 0, 1, true) * 255);
        mWater.draw(&mFboComp.getTexture());
        
        ofPopMatrix();
        RectTransform::end();
        
        //ofSetColor(255); ofEnableBlendMode(OF_BLENDMODE_MULTIPLY); mTexMask.draw(ofGetCurrentViewport()); ofEnableAlphaBlending();
        
        if (mDisplayGui)
        {
            mGui.draw();
        }
    }
    
    void ofApp::nextPressed()
    {
        if (mSlidesDisplay)
        {
            mSlidesDisplay->next();
        }
        else
        {
            mMenuDisplay.next();
        }
    }
    
    void ofApp::prevPressed()
    {
        if (mSlidesDisplay)
        {
            mSlidesDisplay->prev();
        }
        else
        {
            mMenuDisplay.prev();
        }
    }
    
    void ofApp::enterPressed()
    {
        if (mSlidesDisplay)
        {
            mSlidesDisplay->close();
            mNextSlidesDisplay = SlidesDisplayRef();
        }
        else
        {
            mNextSlidesDisplay = SlidesDisplayRef(new SlidesDisplay(mMenuDisplay.mTitles[mMenuDisplay.mCurrentTitleIndex]->mFolder, mOutputView));
        }
        
        mFadeDir = -1;
    }
    
    //--------------------------------------------------------------
    void ofApp::keyPressed(int key)
    {
        if (key == OF_KEY_TAB)
        {
            mDisplayGui ^= 1;
            mDisplayGui ? ofShowCursor() : ofHideCursor();
        }
        
        if (key == OF_KEY_LEFT)
        {
            prevPressed();
        }
        if (key == OF_KEY_RIGHT)
        {
            nextPressed();
        }
        
        if (key == OF_KEY_DOWN)
        {
            enterPressed();
        }
        
        if (key == 's')
        {
            ofXml xml;
            xml.serialize(mParams);
            xml.save("settings.xml");
        }
        
        if (key == 'f')
        {
            ofToggleFullscreen();
        }
    }
    
    void ofApp::onButtonPressedEvent( int & b ) {
        cout << b << endl;
        if(b == 0) {
            prevPressed();
        } else if( b == 1 ) {
            nextPressed();
        } else if( b == 2 ){
            enterPressed();
        }
    }
    
    void ofApp::audioIn(float * input, int bufferSize, int nChannels)
    {
        float curVol = 0.0;
        int numCounted = 0;
#ifdef TARGET_LINUX_ARM
        for (int i = 0; i < bufferSize; i++){
            float left        = input[i]*0.5;
            
            curVol += left * left;
            numCounted++;
        }
#else
        for (int i = 0; i < bufferSize; i++){
            float left        = input[i*2]*0.5;
            float right    = input[i*2+1]*0.5;
            
            curVol += left * left;
            curVol += right * right;
            numCounted+=2;
        }
#endif
        curVol /= (float)numCounted;
        curVol = sqrt( curVol );
        mSmoothedVol *= 0.93;
        mSmoothedVol += 0.07 * curVol;
    }
    
