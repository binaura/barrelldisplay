//
//  MenuTitle.h
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 04..
//

#ifndef MenuTitle_h
#define MenuTitle_h

#include "ofMain.h"
#include "ofxFontStash.h"

typedef std::shared_ptr<class MenuTitle> MenuTitleRef;

class MenuTitle
{
public:
    MenuTitle(string folder);
    void update(bool selected);
    void draw();
    
    string mTitle;
    vector<string> mLines;
    string mFolder;
    ofTexture mTexBg;    
    bool mSelected;
    float mSizePct;
    //static ofTrueTypeFont mFontLarge;
    //static ofTrueTypeFont mFontSmall;
    static ofxFontStash mFont;
    static int mFontSizeLarge;
    static int mFontSizeSmall;
};

#endif /* MenuTitle_h */
