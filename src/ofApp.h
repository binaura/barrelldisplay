#pragma once

#include "ofMain.h"
#include "SlidesDisplay.h"
#include "MenuDisplay.h"
#include "ofxGui.h"
#include "Water.h"
#include "RPi.h"

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    void nextPressed();
    void prevPressed();
    void enterPressed();
    
    void keyPressed(int key);
    void audioIn(float * input, int bufferSize, int nChannels);
    
    ofFbo mFboComp;
    MenuDisplay mMenuDisplay;
    SlidesDisplayRef mSlidesDisplay, mNextSlidesDisplay;
    bool mDisplayGui;
    ofxPanel mGui;
    float mFadeDir, mFadeAlpha;
    
    ofTexture mTexLoading;
    Water mWater;
    ofSoundStream mStream;
    vector <float> mVolHistory;
    float mSmoothedVol;
    ofRectangle mOutputView;
    
    ofParameterGroup mParams;
    ofParameter<float> mScreenRotation;
    ofParameter<float> mVolThreshold;
    ofParameter<float> mWaterDisturbForceMax;
    ofParameter<float> mFadeSpeed;
    
    RPi mRPi;
    void onButtonPressedEvent(int & b);
};

