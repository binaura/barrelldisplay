//
//  MenuDisplay.c
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 04..
//

#include "MenuDisplay.h"
#include "RectTransform.h"

void MenuDisplay::setup(ofRectangle view)
{
    mView = view;
    ofDirectory dir;
    int n = dir.listDir("gallery");
    for (int i = 0; i < n; i++)
    {
        if (dir.getFile(i).isDirectory())
        {
            //string ext = ofToLower(dir.getFile(i).getExtension());
            string folder = dir.getPath(i);
            mTitles.push_back(MenuTitleRef(new MenuTitle(folder)));
        }
    }
    
    mSwipeDamp = 0.04;
    mTitleWidth = 400;
    mPrevTitleIndex = mCurrentTitleIndex = 0;
    mSwipePct = 0;
    mFgFadeTargetAlpha = mFgFadeAlpha = 0;
}

void MenuDisplay::update()
{
    float currentPct = ofMap(mCurrentTitleIndex, 0, mTitles.size() - 1, 0, 1);
    mSwipePct += (currentPct - mSwipePct) * mSwipeDamp;
    mFgFadeAlpha += (mFgFadeTargetAlpha - mFgFadeAlpha) * mSwipeDamp;
    for (int i = 0; i < mTitles.size(); i++)
    {
        mTitles[i]->update(i == mCurrentTitleIndex);
    }
}

void MenuDisplay::draw()
{
    RectTransform::begin(ofRectangle(0, 0, mTitles[mPrevTitleIndex]->mTexBg.getWidth(), mTitles[mPrevTitleIndex]->mTexBg.getHeight()), mView, OF_SCALEMODE_FILL);
    ofSetColor(255);
    mTitles[mPrevTitleIndex]->mTexBg.draw(0, 0);
    RectTransform::end();
    
    RectTransform::begin(ofRectangle(0, 0, mTitles[mCurrentTitleIndex]->mTexBg.getWidth(), mTitles[mCurrentTitleIndex]->mTexBg.getHeight()), mView, OF_SCALEMODE_FILL);
    ofSetColor(255, 255 * mFgFadeAlpha);
    mTitles[mCurrentTitleIndex]->mTexBg.draw(0, 0);
    RectTransform::end();
    
    ofVec2f swipe(ofMap(mSwipePct, 0, 1, mView.getWidth() / 2., mView.getWidth() / 2. - (mTitles.size() - 1) * mTitleWidth), mView.getHeight() / 2.);
    
    ofPushMatrix();
    ofTranslate(swipe);
    for (int i = 0; i < mTitles.size(); i++)
    {
        ofPushMatrix();
        ofTranslate(i * mTitleWidth - mTitles.size() * mTitleWidth, 0);
        mTitles[i]->draw();
        ofPopMatrix();
        
        ofPushMatrix();
        ofTranslate(i * mTitleWidth, 0);
        mTitles[i]->draw();
        ofPopMatrix();
        
        ofPushMatrix();
        ofTranslate(i * mTitleWidth + mTitles.size() * mTitleWidth, 0);
        mTitles[i]->draw();
        ofPopMatrix();
    }
    ofPopMatrix();        
}


void MenuDisplay::next()
{
    mPrevTitleIndex = mCurrentTitleIndex;
    mFgFadeAlpha = 0;
    mFgFadeTargetAlpha = 1;
    
    mCurrentTitleIndex++;
    if (mCurrentTitleIndex >= mTitles.size())
    {
        mCurrentTitleIndex = 0;
        mSwipePct -= 1 + 1 / (float)(mTitles.size() - 1);
    }
    
}

void MenuDisplay::prev()
{
    mPrevTitleIndex = mCurrentTitleIndex;
    mFgFadeAlpha = 0;
    mFgFadeTargetAlpha = 1;
        
    mCurrentTitleIndex--;
    if (mCurrentTitleIndex < 0)
    {
        mCurrentTitleIndex = mTitles.size() - 1;
        mSwipePct += 1 + 1 / (float)(mTitles.size() - 1);
    }
}
