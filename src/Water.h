//
//  Water.h
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 11..
//

#ifndef Water_h
#define Water_h

#include "ofMain.h"



class GridVertex
{
public:
    GridVertex(ofVec2f p, bool fixed);
    void update();
    float mFriction;
    ofVec2f mPos, mVel;
    bool mFixed;
};

class GridSpring
{
public:
    GridSpring(GridVertex * vertexA, GridVertex * vertexB);
    void update();
    float mDst;
    float mDamping;
    GridVertex * mVertexA, * mVertexB;
};

class Water
{
public:
    void setup(int w, int h);
    void update();
    void draw(ofTexture * tex);
    void disturb(ofVec2f p, float radius, float force);
    
    int mGridResX, mGridResY;
    ofVec2f mGridStep;
    ofVec2f mRes;
    ofMesh mMesh;
    vector<GridVertex*> mVertices;
    vector<GridSpring*> mSprings;
};

#endif /* Water_h */
