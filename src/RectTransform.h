#ifndef __ReectTransform__
#define __ReectTransform__

#include "ofMain.h"

class RectTransform
{
public:
    
    static void begin(ofRectangle localView, ofRectangle globalView = ofGetCurrentViewport(), ofScaleMode scaleMode = OF_SCALEMODE_FIT)
    {        
        ofRectangle scaled = localView;
        scaled.scaleTo(globalView, scaleMode);
        ofPushMatrix();
        ofTranslate(scaled.x, scaled.y);
        ofScale(scaled.width / localView.width, scaled.height / localView.height);
    };
    
    static void end(bool useMask = false, ofRectangle localView = ofGetCurrentViewport(), ofRectangle globalView = ofGetCurrentViewport(), ofScaleMode scaleMode = OF_SCALEMODE_FIT)
    {
        ofPopMatrix();
        if (useMask)
        {
            ofRectangle scaled = localView;
            scaled.scaleTo(globalView, scaleMode);
            ofFill();
            ofDrawRectangle(0, 0, ofGetWidth(), scaled.y);
            ofDrawRectangle(0, 0, scaled.x, ofGetHeight());
            ofDrawRectangle(0, scaled.y + scaled.height, ofGetWidth(), ofGetHeight() - scaled.y - scaled.height);
            ofDrawRectangle(scaled.x + scaled.width, 0, ofGetWidth() - scaled.x - scaled.width, ofGetHeight());
        }
    };
    
    static ofVec2f globalToLocal(ofVec2f p, ofRectangle localView, ofRectangle globalView = ofGetCurrentViewport(), ofScaleMode scaleMode = OF_SCALEMODE_FIT)
    {
        ofRectangle scaled = localView;
        scaled.scaleTo(globalView, scaleMode);
        
        return ofVec2f(ofMap(p.x, scaled.x, scaled.x + scaled.width, localView.x, localView.x + localView.width),
                       ofMap(p.y, scaled.y, scaled.y + scaled.height, localView.y, localView.y + localView.height));
    }
    
    static ofVec2f localToGlobal(ofVec2f p, ofRectangle localView, ofRectangle globalView = ofGetCurrentViewport(), ofScaleMode scaleMode = OF_SCALEMODE_FIT)
    {
        ofRectangle scaled = localView;
        scaled.scaleTo(globalView, scaleMode);
        
        return ofVec2f(ofMap(p.x, localView.x, localView.x + localView.width, scaled.x, scaled.x + scaled.width),
                       ofMap(p.y, localView.y, localView.y + localView.height, scaled.y, scaled.y + scaled.height));
    }
};


#endif
