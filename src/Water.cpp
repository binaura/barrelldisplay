//
//  Water.c
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 11..
//

#include "Water.h"

GridVertex::GridVertex(ofVec2f p, bool fixed)
{
    mPos = p;
    mFriction = 0.98;
    mFixed = fixed;
}

void GridVertex::update()
{
    if (!mFixed)
    {
        mVel *= mFriction;
        mPos += mVel;
    }
}

GridSpring::GridSpring(GridVertex * vertexA, GridVertex * vertexB)
{
    mVertexA = vertexA;
    mVertexB = vertexB;
    mDst = mVertexA->mPos.distance(mVertexB->mPos);
    mDamping = 0.05;
}

void GridSpring::update()
{
    ofVec2f v = mVertexA->mPos - mVertexB->mPos;
    float d = v.length();
    v.normalize();
    float dif = mDst - d;
    mVertexA->mVel += v * dif * mDamping;
    mVertexB->mVel -= v * dif * mDamping;
}

void Water::setup(int w, int h)
{
    mRes.set(w, h);
    mGridResX = 40;
    mGridResY = 30;
    
    mGridStep.set(w / (float)(mGridResX - 1), h / (float)(mGridResY - 1));
    
    for (int y = 0; y < mGridResY; y++)
    {
        for (int x = 0; x < mGridResX; x++)
        {
            mVertices.push_back(new GridVertex(ofVec2f(x, y) * mGridStep, !(x > 0 && x < mGridResX - 1 && y > 0 && y < mGridResY - 1)));
        }
    }
    
    for (int y = 0; y < mGridResY - 1; y++)
    {
        for (int x = 0; x < mGridResX - 1; x++)
        {
            int i0 = (x + 0) + (y + 0) * mGridResX;
            int i1 = (x + 1) + (y + 0) * mGridResX;
            int i2 = (x + 0) + (y + 1) * mGridResX;
            int i3 = (x + 1) + (y + 1) * mGridResX;
            
            mSprings.push_back(new GridSpring(mVertices[i0], mVertices[i1]));
            mSprings.push_back(new GridSpring(mVertices[i1], mVertices[i3]));
            mSprings.push_back(new GridSpring(mVertices[i3], mVertices[i2]));
            mSprings.push_back(new GridSpring(mVertices[i2], mVertices[i0]));
            
            mSprings.push_back(new GridSpring(mVertices[i0], mVertices[i3]));
            mSprings.push_back(new GridSpring(mVertices[i1], mVertices[i2]));
        }
    }
}

void Water::update()
{
    
    for (int i = 0; i < mSprings.size(); i++)
    {
        mSprings[i]->update();
    }
    for (int i = 0; i < mVertices.size(); i++)
    {
        mVertices[i]->update();
    }
    
    mMesh.clear();
    mMesh.setMode(OF_PRIMITIVE_TRIANGLES);
    
    for (int y = 0; y < mGridResY - 1; y++)
    {
        for (int x = 0; x < mGridResX - 1; x++)
        {
            int i0 = (x + 0) + (y + 0) * mGridResX;
            int i1 = (x + 1) + (y + 0) * mGridResX;
            int i2 = (x + 0) + (y + 1) * mGridResX;
            int i3 = (x + 1) + (y + 1) * mGridResX;
            
            ofVec2f uv0 = ofVec2f(x + 0, y + 0) * mGridStep / mRes;
            ofVec2f uv1 = ofVec2f(x + 1, y + 0) * mGridStep / mRes;
            ofVec2f uv2 = ofVec2f(x + 0, y + 1) * mGridStep / mRes;
            ofVec2f uv3 = ofVec2f(x + 1, y + 1) * mGridStep / mRes;
            
            mMesh.addVertex(mVertices[i0]->mPos);
            mMesh.addTexCoord(uv0);
            mMesh.addVertex(mVertices[i1]->mPos);
            mMesh.addTexCoord(uv1);
            mMesh.addVertex(mVertices[i3]->mPos);
            mMesh.addTexCoord(uv3);
            
            mMesh.addVertex(mVertices[i0]->mPos);
            mMesh.addTexCoord(uv0);
            mMesh.addVertex(mVertices[i3]->mPos);
            mMesh.addTexCoord(uv3);
            mMesh.addVertex(mVertices[i2]->mPos);
            mMesh.addTexCoord(uv2);
        }
    }
}

void Water::draw(ofTexture * tex)
{
    ofEnableNormalizedTexCoords();
    tex->bind();
    mMesh.draw();
    tex->unbind();
    ofDisableNormalizedTexCoords();
    
    /*
    for (int i = 0; i < mSprings.size(); i++)
    {
        ofDrawLine(mSprings[i]->mVertexA->mPos, mSprings[i]->mVertexB->mPos);
    }
     */
}

void Water::disturb(ofVec2f p, float radius, float force)
{
    float rsq = radius * radius;
    for (int i = 0; i < mVertices.size(); i++)
    {
        float dsq = p.squareDistance(mVertices[i]->mPos);
        if (dsq < rsq)
        {
            float d = sqrtf(dsq);
            float vx = (p.x - mVertices[i]->mPos.x) / d;
            float vy = (p.y - mVertices[i]->mPos.y) / d;
            mVertices[i]->mVel.x -= vx * (radius - d) * force;
            mVertices[i]->mVel.y -= vy * (radius - d) * force;
        }
    }
}
