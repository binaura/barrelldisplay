//
//  SlidesDisplay.h
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 04..
//

#ifndef SlidesDisplay_h
#define SlidesDisplay_h

#include "ofMain.h"
#include "Node.h"

typedef std::shared_ptr<class SlidesDisplay> SlidesDisplayRef;

class SlidesDisplay
{
public:
    SlidesDisplay(string folder, ofRectangle view);
    ~SlidesDisplay();
    void load();
    void update();
    void draw();
    void selectNode(int index);
    void next();
    void prev();
    void close();
    
    vector<NodeRef> mNodes;    
    int mCurrentNodeIndex;
    string mFolder;
    ofRectangle mView;
};

#endif /* SlidesDisplay_h */
