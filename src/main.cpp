#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( )
{
	ofSetupOpenGL(1280, 1024, OF_FULLSCREEN);
	ofRunApp(new ofApp());
}
