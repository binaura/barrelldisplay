#pragma once

#include "ofMain.h"
#ifdef TARGET_LINUX_ARM
    #include "ofxOMXPlayer.h"
#endif
typedef std::shared_ptr<class Node> NodeRef;

class Node
{
public:
    Node(ofVec2f pos, string fileName, ofRectangle view);
    void update();
    void draw();
    void collide(NodeRef other);
    void select();
    void unselect();
    ofPixels convertImageIfNeeded(string fileName);
    
    bool mIsSelected;
    ofVec2f mPos;
    float mRadius, mRadiusMin, mRadiusMax;
    float mCollisionForce;
    float mRadiusGrowDamp;
    float mGravityDampMin, mGravityDampMax;
    ofTexture mTex;
    float mGrowStarted, mGrowInterval;
    bool mIsVideo, mIsPlayingVideo;
    ofRectangle mView;
    string mFileName;
    static ofTexture mTexHighRes;
#ifdef TARGET_LINUX_ARM
    static ofxOMXPlayer * mVideoPlayer;
#else
    static ofVideoPlayer mVideoPlayer;
#endif
    
};

