#pragma once
#ifdef TARGET_LINUX_ARM
    #include "ofxGPIO.h"
#endif

class RPi {
public:
    ofEvent<int> onPressedEvent;
    
#ifdef TARGET_LINUX_ARM
    GPIO gpio12;
    GPIO gpio18;
    GPIO gpio24;
    string s12, s18, s24;
    string p12, p18, p24;
#endif
    
    void setup() {
#ifdef TARGET_LINUX_ARM
        gpio12.setup("12");
        gpio12.export_gpio();
        gpio12.setdir_gpio("out");
        gpio12.setval_gpio("1");
        gpio12.setdir_gpio("in");
        
        gpio18.setup("18");
        gpio18.export_gpio();
        gpio18.setdir_gpio("out");
        gpio18.setval_gpio("1");
        gpio18.setdir_gpio("in");
        
        gpio24.setup("24");
        gpio24.export_gpio();
        gpio24.setdir_gpio("out");
        gpio24.setval_gpio("1");
        gpio24.setdir_gpio("in");
#endif
    }
    
    void update() {
#ifdef TARGET_LINUX_ARM
        gpio12.getval_gpio(s12);
        gpio18.getval_gpio(s18);
        gpio24.getval_gpio(s24);
        if(p12!=s12) {
            int i = 0;
            if(s12 == "0") {
                ofNotifyEvent(onPressedEvent, i);
            }
        }
        if(p18!=s18) {
            int i = 1;
            if(s18 == "0") {
                ofNotifyEvent(onPressedEvent, i);
            }
        }
        if(p24!=s24) {
            int i = 2;
            if(s24 == "0") {
                ofNotifyEvent(onPressedEvent, i);
            }
        }
        
        p12 = s12;
        p18 = s18;
        p24 = s24;
        
        //usleep(50000);
#else
        // running osx
#endif
    }

    void exit(){
#ifdef TARGET_LINUX_ARM
        gpio12.unexport_gpio();
        gpio18.unexport_gpio();
        gpio24.unexport_gpio();
#endif
    }
};
