#include "Node.h"

#ifdef TARGET_LINUX_ARM
    ofxOMXPlayer * Node::mVideoPlayer = NULL;
#else
    ofVideoPlayer Node::mVideoPlayer;
#endif

ofTexture Node::mTexHighRes;

Node::Node(ofVec2f pos, string fileName, ofRectangle view)
{
    mPos = pos;
    mFileName = fileName;
    mView = view;
    
    mIsSelected = false;
    mRadiusMin = 100;
    mRadiusMax = mView.getHeight() * 0.6;
    mCollisionForce = 0.433;
    mRadiusGrowDamp = 0.006;
    mGravityDampMin = 0.001;
    mGravityDampMax = 0.5;
    mRadius = mRadiusMin;
    mGrowInterval = 4;
    mGrowStarted = -1;
    ofFile file(mFileName);
    string ext = ofToLower(file.getExtension());
    if (ext == "jpg")
    {
        mTex.loadData(convertImageIfNeeded(mFileName));
        mIsVideo = false;
    }
    else if (ext == "mp4")
    {
        mIsVideo = true;
        mIsPlayingVideo = false;
        mTex.loadData(convertImageIfNeeded(mFileName + ".thumb"));
    }
    ofLog() << "NODE: " << mFileName + " type : " << (mIsVideo ? "VIDEO" : "IMAGE") << " isLoaded: " << mTex.isAllocated();
}

ofPixels Node::convertImageIfNeeded(string fileName)
{
    ofPixels pixels;
    ofLoadImage(pixels, fileName);
    int maxSize = 256;
    int width = 0;
    int height = 0;
    if (pixels.getWidth() > pixels.getHeight())
    {
        if (pixels.getWidth() > maxSize)
        {
            width = maxSize;
            float ratio = pixels.getHeight() / (float)pixels.getWidth();
            height = width * ratio;
        }
    }
    else
    {
        if (pixels.getHeight() > maxSize)
        {
            height = maxSize;
            float ratio = pixels.getWidth() / (float)pixels.getHeight();
            width = height * ratio;
        }
    }
    
    if (width > 0 && height > 0)
    {
        pixels.resize(width, height);
        //ofSaveImage(pixels, fileName);
        ofLog() << "image " << fileName << " resized to: " << width << " x " << height;
    }
    
    return pixels;
}

void Node::update()
{
    mRadius += ((mIsSelected ? mRadiusMax : mRadiusMin) - mRadius) * mRadiusGrowDamp;
    
    ofVec2f screenCenter = mView.getCenter();
    float pct = ofMap(ofGetElapsedTimef(), mGrowStarted, mGrowStarted + mGrowInterval, 0, 1, true);
    float gravity = ofMap(powf(pct, 2), 0, 1, mGravityDampMin, mGravityDampMax);
    if (mGrowStarted < 0)
    {
        gravity = mGravityDampMin;
    }
    mPos += (screenCenter - mPos) * gravity;
    
    if (mIsPlayingVideo)
    {
#ifdef TARGET_LINUX_ARM
#else
        mVideoPlayer.update();
#endif
    }
}

void Node::select()
{
    ofLog() << "Node::select: type: " << (mIsVideo ? "VIDEO" : "IMAGE");
    mIsSelected = true;
    mIsPlayingVideo = false;
    mGrowStarted = ofGetElapsedTimef();
    
    if (mIsVideo)
    {
#ifdef TARGET_LINUX_ARM
        ofxOMXPlayerSettings settings;
        settings.videoPath = ofToDataPath(mFileName, true);
        settings.useHDMIForAudio = false;    //default true
        settings.enableTexture = true;        //default true
        settings.enableLooping = true;        //default true
        settings.enableAudio = true;        //default true, save resources by disabling
        
        if (mVideoPlayer == NULL)
        {
            mVideoPlayer = new ofxOMXPlayer();
        }
        mVideoPlayer->setup(settings);
        mVideoPlayer->setPaused(false);
        mIsPlayingVideo = true;
#else
        mVideoPlayer.load(mFileName);
        if (mVideoPlayer.isLoaded())
        {
            mVideoPlayer.play();
            mVideoPlayer.setLoopState(OF_LOOP_NORMAL);
            mIsPlayingVideo = true;
        }
#endif
    }
    else
    {
        ofLoadImage(mTexHighRes, mFileName);
    }
}

void Node::unselect()
{
    ofLog() << "Node::unselect: type: " << (mIsVideo ? "VIDEO" : "IMAGE");
    mIsSelected = false;
    mGrowStarted = -1;
    if (mIsPlayingVideo)
    {
        mIsPlayingVideo = false;
#ifdef TARGET_LINUX_ARM
        if (mVideoPlayer != NULL)
        {
            mVideoPlayer->setPaused(true);
            mVideoPlayer->close();
            delete mVideoPlayer;
            mVideoPlayer = NULL;
        }
#else
        mVideoPlayer.close();
#endif
    }
    else
    {
        mTexHighRes.clear();
    }
}

void Node::draw()
{
    ofTexture * tex = NULL;
    
#ifdef TARGET_LINUX_ARM
    
    if (mIsPlayingVideo && mVideoPlayer != NULL)
    {
        tex = &mVideoPlayer->getTextureReference();
    }
#else
    if (mIsPlayingVideo && mVideoPlayer.getTexture().isAllocated())
    {
        tex = &mVideoPlayer.getTexture();
    }
#endif
    else if (mIsSelected)
    {
        tex = &mTexHighRes;
    }
    else
    {
        tex = &mTex;
    }

    ofMesh mesh;
    mesh.setMode(OF_PRIMITIVE_TRIANGLE_FAN);
    
    ofMesh contour;
    contour.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    
    mesh.addVertex(ofVec2f());
    ofVec2f uvCenter = ofVec2f(tex->getWidth() / 2., tex->getHeight() / 2.);
    ofVec2f uvNormalizer = ofVec2f(tex->getWidth(), tex->getHeight());
    mesh.addTexCoord(uvCenter / uvNormalizer);
    int res = 100;
    float angleStep = TWO_PI / (float)(res - 1);
    for (int i = 0; i < res; i++)
    {
        float angle = i * angleStep;
        ofVec2f v = ofVec2f(cos(angle), sin(angle));
        mesh.addVertex(v * mRadius);
        //mMesh.addTexCoord(uvCenter + v * mRadius);
        float maxSize = max(tex->getWidth(), tex->getHeight()) * 0.5;   // max = fit inside ; min = fill whole screen
        if (mIsPlayingVideo)
        {
            maxSize = min(tex->getWidth(), tex->getHeight()) * 0.5;
        }
        mesh.addTexCoord((uvCenter + v * ofMap(mRadius, mRadiusMin, mRadiusMax, maxSize * 0.6, maxSize)) / uvNormalizer);
        
        contour.addVertex(ofVec2f(v * (mRadius - 2)));
        contour.addVertex(ofVec2f(v * (mRadius + 2)));
    }        
    
    ofPushMatrix();
    ofTranslate(mPos);
    ofEnableNormalizedTexCoords();
    tex->bind();
    ofSetColor(255);
    mesh.draw();
    tex->unbind();
    
    ofSetColor(0);
    contour.draw();
    ofDisableNormalizedTexCoords();
    ofPopMatrix();
}

void Node::collide(NodeRef other)
{
    ofVec2f v = other->mPos - mPos;
    float d = v.length();
    float tds = mRadius + other->mRadius;
    if (d < tds)
    {
        float dif = tds - d;
        v.normalize();
        ofVec2f force = v * dif * mCollisionForce;
        mPos -= force;
        other->mPos += force;
    }
}
