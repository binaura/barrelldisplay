//
//  SlidesDisplay.c
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 04..
//

#include "SlidesDisplay.h"

SlidesDisplay::SlidesDisplay(string folder, ofRectangle view)
{
    mFolder = folder;
    mView = view;
}

SlidesDisplay::~SlidesDisplay()
{
    
}

void SlidesDisplay::load()
{
    int maxNumOfSlides = 100;
    float space = 1;
    ofVec2f screenCenter = mView.getCenter();
    ofDirectory dir;
    int n = dir.listDir(mFolder);
    ofLog() << "num files: " << n;
    for (int i = 0; i < min(n, maxNumOfSlides); i++)
    {
        string fileName = dir.getPath(i);
        string ext = ofToLower(dir.getFile(i).getExtension());
        ofLog() << "loading file: " << fileName << " ext: " << ext;
        if (fileName.find("bg.jpg") == std::string::npos)
        {
            if (ext == "jpg" || ext == "mp4")
            {
                mNodes.push_back(NodeRef(new Node(screenCenter + ofVec2f(ofRandomf(), ofRandomf()) * space, fileName, mView)));
            }
        }
    }
    ofLog() << "SlidesDisplay::loaded: " << mNodes.size();
    mCurrentNodeIndex = -1;
    selectNode(0);
}

void SlidesDisplay::update()
{
    for (int i = 0; i < mNodes.size(); i++)
    {
        mNodes[i]->update();
    }
    
    for (int iter = 0; iter < 5; iter++)
    {
        for (int i = 0; i < mNodes.size(); i++)
        {
            for (int j = 0; j < mNodes.size(); j++)
            {
                if (i != j)
                {
                    mNodes[i]->collide(mNodes[j]);
                }
            }
        }
    }
}

void SlidesDisplay::draw()
{    
    for (auto & n : mNodes)
    {
        n->draw();
    }        
}

void SlidesDisplay::selectNode(int index)
{
    ofLog() << "SlidesDisplay::selectNode: " << index;
    
    if (mNodes.size() == 0) return;
    
    if (mCurrentNodeIndex != index)
    {
        mCurrentNodeIndex = index;
        for (auto & n : mNodes)
        {
            n->unselect();
        }
        mNodes[mCurrentNodeIndex]->select();
        ofLog() << "SlidesDisplay::selectNode: unselect all, select: " << mCurrentNodeIndex;
    }
}

void SlidesDisplay::close()
{
    for (auto & n : mNodes)
    {
        n->unselect();
    }
    ofLog() << "SlidesDisplay::close: unselect all";
}

void SlidesDisplay::next()
{
    ofLog() << "SlidesDisplay::next";
    
    if (mNodes.size() == 0) return;
    
    int index = mCurrentNodeIndex + 1;
    if (index >= mNodes.size())
    {
        index = 0;
    }
    selectNode(index);
}

void SlidesDisplay::prev()
{
    ofLog() << "SlidesDisplay::prev";
    
    if (mNodes.size() == 0) return;
    
    int index = mCurrentNodeIndex - 1;
    if (index < 0)
    {
        index =  mNodes.size() - 1;
    }
    selectNode(index);
}

