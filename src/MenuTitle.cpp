//
//  MenuTitle.c
//  BarrelDisplay
//
//  Created by bence samu on 2017. 10. 04..
//

#include "MenuTitle.h"

//ofTrueTypeFont MenuTitle::mFontLarge;
//ofTrueTypeFont MenuTitle::mFontSmall;

ofxFontStash MenuTitle::mFont;
int MenuTitle::mFontSizeLarge = 80;
int MenuTitle::mFontSizeSmall = 60;

MenuTitle::MenuTitle(string folder)
{
    if (mFont.isLoaded() == false)
    {
        mFont.setup("fonts/Oswald-Light.ttf");
    }
    mFolder = folder;
    string text = ofBufferFromFile(mFolder + "/info.txt").getText();
    if (text.size())
    {
        auto lines = ofSplitString(text, "\n");
    
        mTitle = lines[0];
        if (lines.size() > 1)
        {
            for (int i = 1; i < lines.size(); i++)
            {
                mLines.push_back(lines[i]);
            }
        }
    }
    else
    {
        mTitle = "UNTITLED";
    }
    
    ofLoadImage(mTexBg, mFolder + "/bg.jpg");
    mSizePct = 0;
    
}

void MenuTitle::update(bool selected)
{
    mSelected = selected;
    mSizePct += (mSelected - mSizePct) * 0.01;
}

void MenuTitle::draw()
{
    ofPushMatrix();
    float scale = ofMap(mSizePct, 0, 1, 0.3, 1);
    ofScale(scale, scale, scale);
    float linesDescent = 80;
    float lineHeight = 60;
    ofRectangle titleBBox = mFont.getBBox(mTitle, mFontSizeLarge, 0, 0);
    //mFont.getStringBoundingBox(mTitle, 0, 0);
    ofSetColor(255, mSelected ? 255 : 100);
    //mFontLarge.drawString(mTitle, -titleBBox.width / 2., 0);
    mFont.setSize(mFontSizeLarge);
    mFont.drawString(mTitle, -titleBBox.width / 2., 0);
    for (int i = 0; i < mLines.size(); i++)
    {
        ofRectangle lineBBox = mFont.getBBox(mLines[i], mFontSizeSmall, 0, 0);
        //mFontSmall.drawString(mLines[i], -lineBBox.width / 2., -linesDescent - i * lineHeight);
        mFont.setSize(mFontSizeSmall);
        mFont.drawString(mLines[i], -lineBBox.width / 2., linesDescent + i * lineHeight);
    }
    
    ofPopMatrix();
}
